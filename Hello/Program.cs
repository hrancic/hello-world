﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice;
            string stop;
            string[] helloMessages = { "BEGIN DISPLAY(\"HELLO WORLD!\") END.",
                "10 PRINT \"Hello, World!\"\n20 END",
                "#include <stdio.h>\n\nint main(void)\n{\n\tprintf(\"hello, world\\n\");\n}",
                "#include <iostream>\n\nint main()\n{\n \tstd::cout << \"Hello, world!\\n\";\n\treturn 0;'\n}",
                "IDENTIFICATION DIVISION.\nPROGRAM-ID. hello-world.\nPRODEDURE DIVISION.\n \tDISPLAY \"Hello, world!\"",
                "-module(hello).\n-export([hello_world/0]).\n\nhello_world() -> io:fwrite(\"hello, world\\n\").",
                "open System\nConsole.WriteLine(\"Hello World!\")",
                "program helloworld\n\tprint *, \"Hello world!\"\nend program helloworld",
                "package main\nimport \"fmt\"\nfunc main(){\n\tfmt.Println(\"Hello, World\")\n}",
                "module Main where\n\nmain :: IO ()\nmain = putStrln \"Hello, Wrold!\"",
                "class HelloWorldApp{\n\tpublic static void main(String[] args){\n\t\tSystem.out.println(\"Hello World!\");\n\t}\n}",
                "TO HELLO\nPRINT [Hello world]\nEND",
                "program HelloWorld(output);\nbegin\n\tWrite('Hello, world!')\nend.",
                "print(\"Hello World\")",
                "Transcript show: 'Hello World!'"
                };


            do
            {  
                System.Console.WriteLine("Select programming language to see how 'Hello World' is written in that language.");
                System.Console.WriteLine("1. ALGOL\n" +
                                         "2. BASIC\n" +
                                         "3. C\n" +
                                         "4. C++\n" +
                                         "5. COBOL\n" +
                                         "6. ERLANG\n" +
                                         "7. F#\n" +
                                         "8. FORTRAN\n" +
                                         "9. GO\n" +
                                         "10. HASKELL\n" +
                                         "11. JAVA\n" +
                                         "12. LOGO\n" +
                                         "13. PASCAL\n" +
                                         "14. PYTHON\n" +
                                         "15. SMALLTALK\n");

                do
                {
                    System.Console.Write("Pick a number from 1 to 15: ");
                    choice = int.Parse(System.Console.ReadLine());
                    
                    if(choice < 1 || choice > 15)
                    {
                        System.Console.WriteLine("Are you sure that number " + choice + " is between 1 and 15?");
                    }
                } while (choice < 1 || choice > 15);
                

                System.Console.WriteLine("\n" + helloMessages[choice - 1] + "\n");

                System.Console.Write("Wanna see another Hello World? y/n ");
                stop = System.Console.ReadLine();

            } while ( stop == "y" || stop == "Y" );
        }
    }
}
